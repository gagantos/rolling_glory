const { Pool } = require('pg')
require('dotenv').config();
const ObjConn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    ssl: { rejectUnauthorized: false }
}

const pool = new Pool(ObjConn);

// pool.connect(err => {
//     if (err) {
//         console.error('Database error connecting', err.stack)
//     } else {
//         console.log('Database Postgresql connected')
//     }
// });

const db = require('knex')({
    client: 'pg',
    connection: ObjConn,

    searchPath: ['test_rolling'],
    pool: {
        min: 0,
        max: 10,
    }
});

module.exports = db