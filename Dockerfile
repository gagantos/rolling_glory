FROM node:14-slim

# Create app directory
WORKDIR /usr/src/app

ENV TZ=Etc/UTC

RUN echo $TZ > /etc/timezone && \
    echo "deb http://http.debian.net/debian/ stretch main contrib non-free" > /etc/apt/sources.list && \
    echo "deb http://http.debian.net/debian/ stretch-updates main contrib non-free" >> /etc/apt/sources.list && \
    echo "deb http://security.debian.org/ stretch/updates main contrib non-free" >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y tzdata libaio1 unzip wget gnupg bzip2 \
    ttf-mscorefonts-installer fontconfig \
    fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf && \
    wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip && \
    unzip instantclient-basiclite-linuxx64.zip && \
    rm -f instantclient-basiclite-linuxx64.zip && \
    cd instantclient* && \
    rm -f *jdbc* *occi* *mysql* *jar uidrvci genezi adrci && \
    echo /usr/src/app/instantclient* > /etc/ld.so.conf.d/oracle-instantclient.conf && \
    ldconfig && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get clean

# COPY 51-local.conf /etc/fonts/conf.d/51-local.conf
RUN fc-cache -f -v

# Install app dependencies
COPY package.json ./

RUN npm i

# Bundle app source
COPY . .
COPY .env.docker .env

EXPOSE 3500
CMD [ "node", "./bin/www" ]