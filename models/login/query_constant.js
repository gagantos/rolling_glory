const { db } = require('../../config/index');
const crypto = require('crypto')
const token = require('jsonwebtoken')

const login_query = {
    registerQuery: () => {

    },
    loginQuery: (data) => {
        const { username, password } = data
        const query = db({ a: "rolling_gifts.t_profile_data" })
            .where({ username_profile: username, password_profile: password })
            .toString();
        return query;
    }
}

module.exports = login_query