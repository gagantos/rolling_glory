const { db } = require('../../config/index');
const crypto = require('crypto')
const { getrows, execQuery } = require('../../helpers')

const gifts_query = {
    query_get_gifts: (data) => {
        const { id_gifts, id_profile, limit, page } = data
        const query = db({ a: "rolling_gifts.t_gifts_data" });
        query.select(["a.*", "b.stok", "c.skor_rating"])
        query.leftJoin({ b: "rolling_gifts.t_gifts_data_stok" }, (onClause) => {
            onClause.on("a.id_gifts", "b.id_gifts");
        })

        if (id_profile) {
            query.leftJoin({ c: "rolling_gifts.t_profile_rating_gifts" }, (data) => {
                data.on("a.id_gifts", "c.id_gifts")
                    .on("a.id_profile", id_profile)
            })
        } else {
            query.leftJoin({ c: "rolling_gifts.t_profile_rating_gifts" }, (data) => {
                data.on("a.id_gifts", "c.id_gifts")
            })
        }

        if (id_gifts) {
            query.where(function () {
                this.where('a.id_gifts', id_gifts)
            })
        }

        if (limit) {
            query.limit(limit)
        }

        if (page) {
            query.offset((parseInt(page) - 1) * limit)
        }
        console.log(query.toString());
        return query.toString()
    },
    query_get_redeem: (data) => {
        const { id_gifts, id_profile, limit, page } = data
        const query = db({ a: "rolling_gifts.t_gifts_redeem" });
        query.select(["a.*"])
        if (id_gifts) {
            query.where(function () {
                this.where('a.id_gifts', id_gifts)
            })
        }

        if (id_profile) {
            query.where(function () {
                this.where('a.id_profile', id_profile)
            })
        }

        if (limit) {
            query.limit(limit)
        }

        if (page) {
            query.offset((parseInt(page) - 1) * limit)
        }
        console.log(query.toString());
        return query.toString()
    },
    query_get_ratings: (data) => {
        const { id_gifts, id_profile, limit, page } = data
        const query = db({ a: "rolling_gifts.t_profile_rating_gifts" });
        query.select(["a.*"])
        if (id_gifts) {
            query.where(function () {
                this.where('a.id_gifts', id_gifts)
            })
        }

        if (id_profile) {
            query.where(function () {
                this.where('a.id_profile', id_profile)
            })
        }

        if (limit) {
            query.limit(limit)
        }

        if (page) {
            query.offset((parseInt(page) - 1) * limit)
        }
        console.log(query.toString());
        return query.toString()
    },
    query_get_stok: (data) => {
        const { id_gifts } = data
        const query = db({ a: "rolling_gifts.t_gifts_data_stok" });
        query.select(["a.*"])
        query.where({ id_gifts: id_gifts })
        return query.toString();
    },
    insert_update_gifts: (data, id_gifts_new) => {
        const {
            id_gifts,
            nama_gifts,
            detail_gifts,
            kategori_gifts,
            id_profile_added_gifts,
            is_available,
            is_best_seller,
            is_hot_item,
            img_gifts,
        } = data

        const databody = {
            id_gifts: id_gifts,
            nama_gifts: nama_gifts,
            detail_gifts: detail_gifts,
            kategori_gifts: kategori_gifts,
            id_profile_added_gifts: id_profile_added_gifts,
            is_available: is_available,
            is_best_seller: is_best_seller,
            is_hot_item: is_hot_item,
            img_gifts: img_gifts,
        }
        const query = db({ a: "rolling_gifts.t_gifts_data" });
        if (!id_gifts_new) {
            query.insert(databody)
        } else {
            query.update(databody)
                .where('id_gifts', id_gifts_new)
        }

        return query.toString();
    },
    insert_update_gifts_stok: (data, id_stok_upd) => {
        const {
            id_gifts,
            id_stok,
            stok,
        } = data

        const databody = {
            id_gifts: id_gifts,
            id_stok: id_stok,
            stok: stok,
        }
        const query = db({ a: "rolling_gifts.t_gifts_data_stok" });
        if (!id_stok_upd) {
            query.insert(databody)
        } else {
            query.update(databody)
                .where('id_stok', id_stok_upd)
        }

        return query.toString();
    },
    delete_gifts: (data) => {
        const { id_gifts } = data
        const query = db({ a: "rolling_gifts.t_gifts_data" });
        return query.del().where({ id_gifts: id_gifts }).toString();
    },
    insert_update_redeem: async (data) => {
        const { id_gifts, id_profile, id_redeem, nomor_redeem, trx } = data
        console.log(data);
        let datastok = db({ a: "rolling_gifts.t_gifts_data_stok" })
            .update({ stok: db.raw('?? - 1', ['stok']) })
            .where({ id_gifts })
            .toString();
        console.log(datastok)
        await trx.raw(datastok)
        let data_redeem = db({ a: "rolling_gifts.t_gifts_redeem" })
            .insert({
                id_redeem: id_redeem,
                id_gifts: id_gifts,
                nomor_unique_redeem: nomor_redeem,
                id_profile: id_profile
            })
            .toString();

        await trx.raw(data_redeem)
        return true;
    },
    insert_update_rating: async (data, id_profile_rating) => {
        const { id_gifts, id_profile, id_profile_rating_new, rating, trx } = data
        let data_redeem;
        console.log()
        if (id_profile_rating) {
            data_redeem = db({ a: "rolling_gifts.t_profile_rating_gifts" })
                .update({
                    skor_rating: rating
                })
                .where({ id_profile_rating })
                .toString();
        } else {
            data_redeem = db({ a: "rolling_gifts.t_profile_rating_gifts" })
                .insert({
                    skor_rating: rating,
                    id_profile,
                    id_profile_rating: id_profile_rating_new,
                    id_gifts
                })
                .toString();
        }


        await trx.raw(data_redeem)
        return true;
    }
}

module.exports = gifts_query