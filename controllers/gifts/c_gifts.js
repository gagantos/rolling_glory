const db = require('../../config/pg_conn_knex')
const queryGifts = require('../../models/gifts/query_constant')
const { getrows, execQuery } = require('../../helpers')
const crypto = require('crypto')
const gifts_query = require('../../models/gifts/query_constant')

const C_gifts = {
    getData: async (req, res, next) => {
        try {
            let id_profile_auth = res.locals.id_profile
            const { id_gifts, jenis_act } = req.params
            let msgInfo = ""
            switch (jenis_act) {
                case "redeem":
                    await redeemClass.insertUpdateRedeem(id_gifts, id_profile_auth)
                    msgInfo = "Berhasil meredeem gifts"
                    break;
                case "rating":
                    const { rating } = req.body;
                    let objrating = {
                        id_gifts,
                        id_profile_auth,
                        rating: rating
                    }
                    await ratingClass.insertUpdateRating(objrating)
                    msgInfo = "Berhasil menambah kan rating"
                    break;
                default:
                    const { page, limit } = req.query
                    // const { id_gifts } = req.header
                    let data = { id_gifts, page, limit }
                    const query = queryGifts.query_get_gifts(data);
                    const arr = await getrows(query, true);
                    console.log(arr);
                    msgInfo = arr
                    break;
            }
            res.status(200).json({
                kode: 200,
                msg: msgInfo
            })

        } catch (error) {
            res.status(500).json({
                kode: 500,
                status: "err",
                msg: error.message,
                detail: error.stack
            })
        }
    },
    updateData: async (req, res, next) => {
        try {
            const { id_gifts } = req.params
            const {
                nama_gifts,
                detail_gifts,
                kategori_gifts,
                id_profile_added_gifts,
                is_available,
                is_best_seller,
                is_hot_item,
                img_gifts,
                stok
            } = req.body

            let data = {
                nama_gifts,
                detail_gifts,
                kategori_gifts,
                id_profile_added_gifts,
                is_available,
                is_best_seller,
                is_hot_item,
                img_gifts,
                stok
            }

            const querygif = queryGifts.insert_update_gifts(data, id_gifts);
            await execQuery(querygif);
            const querycheckstok = queryGifts.query_get_stok({ id_gifts: id_gifts });
            console.log(querycheckstok);
            const querystok = await getrows(querycheckstok);
            let dataStok, queryst;
            if (Object.keys(querystok).length > 0) {
                idstoknew = querystok.id_stok;
                dataStok = { stok: stok };
                queryst = queryGifts.insert_update_gifts_stok(dataStok, idstoknew);
            } else {
                dataStok = { id_stok: idstoknew, id_gifts: id_gifts, stok: stok }
                queryst = queryGifts.insert_update_gifts_stok(dataStok);
            }
            await execQuery(queryst);
            res.status(200).json({
                kode: 200,
                msg: "Berhasil mengubah data Gifts"
            })

        } catch (error) {
            res.status(500).json({
                kode: 500,
                status: "err",
                msg: error.stack
            })
        }
    },
    insertData: async (req, res, next) => {
        try {
            // const { id_gifts } = req.header
            const idgiftsnew = `RGT-${crypto.randomBytes(20).toString('hex')}`;
            const idstoknew = `ST-${crypto.randomBytes(20).toString('hex')}`;
            let data = { id_gifts: idgiftsnew, ...req.body }
            const querygif = queryGifts.insert_update_gifts(data);
            await execQuery(querygif);
            let dataStok = { id_stok: idstoknew, id_gifts: idgiftsnew, stok: req.body.stok }
            const queryst = queryGifts.insert_update_gifts_stok(dataStok);
            await execQuery(queryst);
            res.status(200).json({
                kode: 200,
                msg: "Berhasil menambahkan data gifts"
            })

        } catch (error) {
            res.status(500).json({
                kode: 500,
                status: "err",
                msg: error.stack
            })
        }
    },
    deleteData: async (req, res, next) => {
        try {
            const { id_gifts } = req.params
            // const { id_gifts } = req.header
            let data = { id_gifts }
            const query = queryGifts.delete_gifts(data);
            console.log(query);
            const arr = await execQuery(query);
            console.log(arr);
            res.status(200).json({
                kode: 200,
                msg: "Berhasil menghapus data gifts"
            })

        } catch (error) {
            res.status(500).json({
                kode: 500,
                status: "err",
                msg: "Gagal menghapus data gifts"
            })
        }
    }
}

const redeemClass = {
    insertUpdateRedeem: async (id_gifts, id_profile) => {
        const trx = await db.transaction();
        let data = { id_gifts, id_profile }
        const query = queryGifts.query_get_gifts(data);
        const arr = await getrows(query);
        if (Object.keys(arr).length > 0) {
            if (arr.is_available < 1) {
                throw new Error("Stok sudah habis silahkan memilih barang lain")
            }

            const id_redeem = `RDM-${crypto.randomBytes(20).toString('hex')}`;
            const nop = `NOP${crypto.randomBytes(20).toString('hex')}`;
            const nomor_redeem = `${crypto.randomBytes(10).toString('hex')}`;

            let dataIns = {
                id_gifts,
                id_redeem,
                id_profile,
                nomor_redeem,
                trx,
                nop
            }
            await gifts_query.insert_update_redeem(dataIns);
            await trx.commit();
        } else {
            await trx.rollback();
            throw new Error("Data gifts tidak ditemukan. Silahkan muat ulang")
        }

    }
}

const ratingClass = {
    insertUpdateRating: async (datanew) => {
        const trx = await db.transaction();
        const { id_gifts, id_profile_auth, rating } = datanew;
        let data = { id_gifts, id_profile: id_profile_auth }
        const query = queryGifts.query_get_redeem(data);
        const arr = await getrows(query);
        if (Object.keys(arr).length > 0) {

            const query = queryGifts.query_get_ratings(data);
            console.log(query);
            const arr = await getrows(query);
            const id_profile_rating_new = `RT-${crypto.randomBytes(20).toString('hex')}`;
            let dataIns = {
                id_profile_rating_new,
                id_gifts,
                id_profile: id_profile_auth,
                trx,
                rating
            }
            if (Object.keys(arr).length > 0) {
                await gifts_query.insert_update_rating(dataIns, arr.id_profile_rating);
            } else {
                await gifts_query.insert_update_rating(dataIns);
            }
            await trx.commit();
        } else {
            await trx.rollback();
            throw new Error("Data gifts belum di redeem. Tidak dapat memberikan ratings")
        }
    }
}


module.exports = C_gifts