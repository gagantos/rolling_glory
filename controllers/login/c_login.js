const db = require('../../config/pg_conn_knex')
const queryLogin = require('../../models/login/query_constant')
const { getrows, jwttoken, execQuery } = require('../../helpers')
const md5 = require('md5')


const C_login = {
    loginUser: async (req, res, next) => {
        try {
            const { username, password } = req.body
            // const { id_gifts } = req.header
            let data = { username, password: md5(password) }
            const query = queryLogin.loginQuery(data);
            const arr = await getrows(query);
            if (Object.keys(arr).length < 1) {
                throw Error("Username atau password salah")
            }

            let objToken = {
                id_profile: arr.id_profile,
                username: arr.username_profile,
            }
            let generateToken = await jwttoken.setToken(objToken)
            // let d = await jwttoken.decodeToken('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9wcm9maWxlIjoxLCJpYXQiOjE2NDI2ODI2NjgsImV4cCI6MTY0MjY4NDQ2OH0.5jT5WIpZIzV4BmWJfs-lGads3DHRyv4LDnS9408ZIF8')
            res.status(200).json({
                kode: 200,
                msg: generateToken
            })
        } catch (error) {
            res.status(500).json({
                kode: 500,
                status: "err",
                msg: error.message
            })
        }
    },
}

module.exports = C_login