var express = require('express');
var router = express.Router();
const _r_gifts = require('./gifts/r_index_gifts')
const _r_login = require('./login/r_index_login')
// const _r_users = require('./users/r_index_users')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});
router.use('/gifts', _r_gifts);
router.use('/login', _r_login);
// router.use('/users', _r_users);

module.exports = router;
