var express = require('express');
var router = express.Router();
var _c_login = require('../../controllers/login/c_login')

router.get('/', _c_login.loginUser);

module.exports = router