var express = require('express');
var router = express.Router();
var _c_gifts = require('../../controllers/gifts/c_gifts')
var { check_login } = require('../../helpers/checkIsLogin')

router.get('/:id_gifts?', _c_gifts.getData);
// router.post('/',);
router.post('/', check_login, _c_gifts.insertData);
router.post('/:id_gifts?/:jenis_act?', check_login, _c_gifts.getData);
router.put('/:id_gifts', check_login, _c_gifts.updateData);
router.patch('/:id_gifts', check_login, _c_gifts.updateData);
router.delete('/:id_gifts', check_login, _c_gifts.deleteData);

module.exports = router