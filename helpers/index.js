const getrows = require('./getRows')
const getprofile = require('./getProfile')
const checkislogin = require('./checkIsLogin')
const execQuery = require('./execQuery')
const jwttoken = require('./jwtToken')

module.exports = {
    getrows,
    execQuery,
    jwttoken,
    getprofile,
    checkislogin
}

