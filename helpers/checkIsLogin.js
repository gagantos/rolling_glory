const jwt = require('./jwtToken')
const moment = require('moment-timezone')

const checklogin = {
    check_login: async (req, res, next) => {
        try {
            const token = req.header('token')
            console.log(token);
            const data = await jwt.decodeToken(token)
            res.locals.id_profile = data.id_profile;
            next()
        } catch (error) {
            console.log(error.message);
            console.log(error.stack);
            let objerror = {
                kode: 500,
                msg: "Token tidak valid. Silahkan login terlebih dahulu"
            }
            if (error.code) {
                objerror.msg = error.message;
                objerror.kode = error.code;
            }
            res.status(500).json(objerror)
        }

    }
}

module.exports = checklogin