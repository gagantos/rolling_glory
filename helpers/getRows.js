const { db } = require('../config')

module.exports = async (query, many) => db
    .raw(query)
    .then((res) => {

        if (many) {
            return (res.rowCount === 0) ? [] : res.rows;
        } else {
            return (res.rowCount === 0) ? {} : res.rows[0];
        }
    })
    .catch((e) => {
        console.error(e.stack);
        throw new Error(error.message);
    })
