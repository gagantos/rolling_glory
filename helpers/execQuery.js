const { db } = require('../config')

module.exports = async (query) => db
    .raw(query)
    .then((res) => {
        return res;
    })
    .catch((e) => {
        console.error(e.stack);
        reject(e)
        throw new Error(error.message);
    })
