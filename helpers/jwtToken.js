const { db } = require('../config/index');
const token = require('jsonwebtoken')
require('dotenv').config();

const jwttoken = {
    setToken: async (data) => {
        const jwt = token.sign(data, process.env.JWT_AUTH, { expiresIn: process.env.JWT_EXPIRE });
        return jwt
    },
    decodeToken: async (data) => {
        try {
            var decoded = token.verify(data, process.env.JWT_AUTH);
            return decoded;
        } catch (err) {
            console.log(err.message);
            throw new Error("Token tidak valid. Silahkan lakukan login ulang")
        }
    }
}

module.exports = jwttoken
