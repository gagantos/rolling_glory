/*
 Navicat PostgreSQL Data Transfer

 Source Server         : postgres_sendiri
 Source Server Type    : PostgreSQL
 Source Server Version : 130002
 Source Host           : localhost:5432
 Source Catalog        : test_rolling
 Source Schema         : rolling_gifts

 Target Server Type    : PostgreSQL
 Target Server Version : 130002
 File Encoding         : 65001

 Date: 21/01/2022 14:15:20
*/


-- ----------------------------
-- Table structure for m_master_reff
-- ----------------------------
DROP TABLE IF EXISTS "rolling_gifts"."m_master_reff";
CREATE TABLE "rolling_gifts"."m_master_reff" (
  "grup" varchar(100) COLLATE "pg_catalog"."default",
  "id" int4,
  "uraian" varchar(100) COLLATE "pg_catalog"."default",
  "uraian_pendek" varchar(100) COLLATE "pg_catalog"."default",
  "urutan" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamp(6),
  "updated" timestamp(6)
)
;

-- ----------------------------
-- Records of m_master_reff
-- ----------------------------
INSERT INTO "rolling_gifts"."m_master_reff" VALUES ('kategori_gifts', 1, 'Gadget', NULL, '1', '2022-01-20 13:06:38', '2022-01-20 13:06:38');
INSERT INTO "rolling_gifts"."m_master_reff" VALUES ('kategori_gifts', 2, 'Perabotan Rumah Tangga', NULL, '2', '2022-01-20 13:06:42', '2022-01-20 13:06:42');

-- ----------------------------
-- Table structure for m_master_role
-- ----------------------------
DROP TABLE IF EXISTS "rolling_gifts"."m_master_role";
CREATE TABLE "rolling_gifts"."m_master_role" (
  "id_role" varchar(3) COLLATE "pg_catalog"."default" NOT NULL,
  "uraian_role" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamp(6),
  "updated" timestamp(6)
)
;

-- ----------------------------
-- Records of m_master_role
-- ----------------------------

-- ----------------------------
-- Table structure for t_gifts_data
-- ----------------------------
DROP TABLE IF EXISTS "rolling_gifts"."t_gifts_data";
CREATE TABLE "rolling_gifts"."t_gifts_data" (
  "id_gifts" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "nama_gifts" varchar(255) COLLATE "pg_catalog"."default",
  "detail_gifts" text COLLATE "pg_catalog"."default",
  "jenis_gifts" int4,
  "kategori_gifts" int4,
  "img_gifts" varchar(100) COLLATE "pg_catalog"."default",
  "id_profile_added_gifts" int4,
  "created" timestamp(6) DEFAULT now(),
  "updated" timestamp(6) DEFAULT now(),
  "is_available" int2,
  "is_best_seller" int2,
  "is_hot_item" int2
)
;

-- ----------------------------
-- Records of t_gifts_data
-- ----------------------------
INSERT INTO "rolling_gifts"."t_gifts_data" VALUES ('RG-1293129391322123', 'Samsung Galaxy S9 -Midnight Black 4/64 GB', 'Ukuran layar: 6.2 inci, Dual Edge Super AMOLED 2960 x 1440 (Quad HD+) 529 ppi, 18.5:9 Memori: RAM 6 GB (LPDDR4), ROM 64 GB, MicroSD up to 400GB Sistem operasi: Android 8.0 (Oreo) CPU: Exynos 9810 Octa-core (2.7GHz Quad + 1.7GHz Quad), 64 bit, 10nm processor Kamera: Super Speed Dual Pixel, 12 MP OIS (F1.5/F2.4 Dual Aperture) + 12MP OIS (F2.4) with LED flash, depan 8 MP, f/1.7, autofocus, 1440p@30fps, dual video call, Auto HDR SIM: Dual SIM (Nano-SIM) Baterai: Non-removable Li-Ion 3500 mAh , Fast Charging on wired and wireless', 1, 1, NULL, 1, '2022-01-20 12:53:52', '2022-01-20 12:53:54', 1, 1, 0);
INSERT INTO "rolling_gifts"."t_gifts_data" VALUES ('RGT-fc76b5f0a84621fb6026e12f80aa274df7bee94f', 'Namaa COBABAAA', 'Ini adalah contoh Detail', NULL, 1, NULL, 1, '2022-01-20 16:36:15.146915', '2022-01-20 16:36:15.146915', 1, 0, 0);
INSERT INTO "rolling_gifts"."t_gifts_data" VALUES ('RGT-0682e7a3e881e410993e16d081c71e9b7c4ea485', 'Namaa', 'Ini adalah contoh Detail', NULL, 1, '', 1, '2022-01-21 01:53:04.132279', '2022-01-21 01:53:04.132279', 0, 0, 0);

-- ----------------------------
-- Table structure for t_gifts_data_stok
-- ----------------------------
DROP TABLE IF EXISTS "rolling_gifts"."t_gifts_data_stok";
CREATE TABLE "rolling_gifts"."t_gifts_data_stok" (
  "id_stok" varchar(60) COLLATE "pg_catalog"."default" NOT NULL,
  "id_gifts" varchar(60) COLLATE "pg_catalog"."default",
  "stok" int2 DEFAULT 0,
  "created" timestamp(6) DEFAULT now(),
  "updated" timestamp(6) DEFAULT now()
)
;

-- ----------------------------
-- Records of t_gifts_data_stok
-- ----------------------------
INSERT INTO "rolling_gifts"."t_gifts_data_stok" VALUES ('ST-ad1887bef6e615b62b13f866a902e9bed4283193', 'RGT-fc76b5f0a84621fb6026e12f80aa274df7bee94f', 32, '2022-01-20 16:36:15.152882', '2022-01-20 16:36:15.152882');
INSERT INTO "rolling_gifts"."t_gifts_data_stok" VALUES ('ST-120391203910239123', 'RG-12931293913209', 0, '2022-01-20 13:07:45', '2022-01-20 13:07:47');
INSERT INTO "rolling_gifts"."t_gifts_data_stok" VALUES ('ST-0c8997b5fd6e3389559bfac038b27d48d0483670', 'RGT-0682e7a3e881e410993e16d081c71e9b7c4ea485', 0, '2022-01-21 01:53:04.143377', '2022-01-21 01:53:04.143377');

-- ----------------------------
-- Table structure for t_gifts_redeem
-- ----------------------------
DROP TABLE IF EXISTS "rolling_gifts"."t_gifts_redeem";
CREATE TABLE "rolling_gifts"."t_gifts_redeem" (
  "id_redeem" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "id_gifts" varchar(100) COLLATE "pg_catalog"."default",
  "nomor_unique_redeem" varchar(255) COLLATE "pg_catalog"."default",
  "id_profile" int4,
  "created" varchar(255) COLLATE "pg_catalog"."default",
  "updated" date
)
;

-- ----------------------------
-- Records of t_gifts_redeem
-- ----------------------------
INSERT INTO "rolling_gifts"."t_gifts_redeem" VALUES ('RDM-fb2080c76d8918d826aec7c35107f52205e53d9d', 'RGT-fc76b5f0a84621fb6026e12f80aa274df7bee94f', '60f9274f2734f4d144b7', 1, NULL, NULL);

-- ----------------------------
-- Table structure for t_profile_data
-- ----------------------------
DROP TABLE IF EXISTS "rolling_gifts"."t_profile_data";
CREATE TABLE "rolling_gifts"."t_profile_data" (
  "id_profile" int4 NOT NULL,
  "nama_profile" varchar(100) COLLATE "pg_catalog"."default",
  "username_profile" varchar(200) COLLATE "pg_catalog"."default",
  "password_profile" varchar(255) COLLATE "pg_catalog"."default",
  "created" timestamp(6),
  "updated" timestamp(6)
)
;

-- ----------------------------
-- Records of t_profile_data
-- ----------------------------
INSERT INTO "rolling_gifts"."t_profile_data" VALUES (1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL);

-- ----------------------------
-- Table structure for t_profile_rating_gifts
-- ----------------------------
DROP TABLE IF EXISTS "rolling_gifts"."t_profile_rating_gifts";
CREATE TABLE "rolling_gifts"."t_profile_rating_gifts" (
  "id_profile_rating" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "id_profile" int4,
  "skor_rating" float4,
  "updated" timestamp(6) DEFAULT now(),
  "created" timestamp(6) DEFAULT now(),
  "id_gifts" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of t_profile_rating_gifts
-- ----------------------------
INSERT INTO "rolling_gifts"."t_profile_rating_gifts" VALUES ('RTI-19238129381329', 1, 3.523, '2022-01-20 14:19:25', '2022-01-20 14:19:30.151487', 'RG-1293129391322123');
INSERT INTO "rolling_gifts"."t_profile_rating_gifts" VALUES ('RT-f0524b4bca849fe125407c69af0596311a4a9fc9', 1, 2.55, '2022-01-21 07:37:12.683977', '2022-01-21 07:37:12.683977', 'RGT-fc76b5f0a84621fb6026e12f80aa274df7bee94f');

-- ----------------------------
-- Table structure for t_profile_role_data
-- ----------------------------
DROP TABLE IF EXISTS "rolling_gifts"."t_profile_role_data";
CREATE TABLE "rolling_gifts"."t_profile_role_data" (
  "id" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "role_user" varchar(3) COLLATE "pg_catalog"."default",
  "id_profile" int4,
  "created" timestamp(6),
  "updated" timestamp(6)
)
;

-- ----------------------------
-- Records of t_profile_role_data
-- ----------------------------
INSERT INTO "rolling_gifts"."t_profile_role_data" VALUES ('RLE-19283192381938', '01', 1, NULL, NULL);

-- ----------------------------
-- Function structure for update_is_available
-- ----------------------------
DROP FUNCTION IF EXISTS "rolling_gifts"."update_is_available"();
CREATE OR REPLACE FUNCTION "rolling_gifts"."update_is_available"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
	declare
		data varchar(100);
	BEGIN
		IF NEW.stok < 0 
			THEN RAISE EXCEPTION 'Stok Kosong';
		END IF;
		IF NEW.stok = 0 THEN 
			UPDATE rolling_gifts.t_gifts_data SET is_available = 0 WHERE id_gifts = OLD.id_gifts;
		END IF;
		RETURN NEW;
	END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Uniques structure for table m_master_reff
-- ----------------------------
ALTER TABLE "rolling_gifts"."m_master_reff" ADD CONSTRAINT "grup_reff" UNIQUE ("grup", "id");

-- ----------------------------
-- Primary Key structure for table m_master_role
-- ----------------------------
ALTER TABLE "rolling_gifts"."m_master_role" ADD CONSTRAINT "m_master_role_pkey" PRIMARY KEY ("id_role");

-- ----------------------------
-- Primary Key structure for table t_gifts_data
-- ----------------------------
ALTER TABLE "rolling_gifts"."t_gifts_data" ADD CONSTRAINT "t_gifts_data_pkey" PRIMARY KEY ("id_gifts");

-- ----------------------------
-- Triggers structure for table t_gifts_data_stok
-- ----------------------------
CREATE TRIGGER "query_update_and_checkl" BEFORE UPDATE OF "stok" ON "rolling_gifts"."t_gifts_data_stok"
FOR EACH ROW
EXECUTE PROCEDURE "rolling_gifts"."update_is_available"();

-- ----------------------------
-- Primary Key structure for table t_gifts_data_stok
-- ----------------------------
ALTER TABLE "rolling_gifts"."t_gifts_data_stok" ADD CONSTRAINT "t_gifts_data_stok_pkey" PRIMARY KEY ("id_stok");

-- ----------------------------
-- Primary Key structure for table t_gifts_redeem
-- ----------------------------
ALTER TABLE "rolling_gifts"."t_gifts_redeem" ADD CONSTRAINT "t_gifts_redeem_pkey" PRIMARY KEY ("id_redeem");

-- ----------------------------
-- Primary Key structure for table t_profile_data
-- ----------------------------
ALTER TABLE "rolling_gifts"."t_profile_data" ADD CONSTRAINT "t_profile_data_pkey" PRIMARY KEY ("id_profile");

-- ----------------------------
-- Primary Key structure for table t_profile_rating_gifts
-- ----------------------------
ALTER TABLE "rolling_gifts"."t_profile_rating_gifts" ADD CONSTRAINT "t_profile_rating_gifts_pkey" PRIMARY KEY ("id_profile_rating");

-- ----------------------------
-- Primary Key structure for table t_profile_role_data
-- ----------------------------
ALTER TABLE "rolling_gifts"."t_profile_role_data" ADD CONSTRAINT "t_profile_role_data_pkey" PRIMARY KEY ("id");
