### Note: Password untuk login untuk admin adalah `u: admin, p: admin`
## 1.a Instalasi
1. Lakukan clone pada repo ini.
2. Pastikan sudah menginstall Database Postgresql pada sistem.
3. Jalankan aplikasi database postgresqlnya.
4. Exports terlebih dahulu file skema databasenya pada folder `./docs` dengan nama file `rolling_gifts.sql`
5. Set `.env` dengan melihat contoh pada `.env.example`.
6. Setting database postgresql dengan username dan password sesuai dengan database local Anda.
7. Jalankan `npm install`
## 2.a Menjalankan server secara local
1. Jika sudah menjalankan semua yang ada pada **1.a Instalasi**, jalankan `npm start`
2. Akses `localhost:3500/`, jika tidak ada error maka akan tampil database yang ada

## 1.b Instalasi

1. Pastikan anda telah menginstal aplikasi docker dan dapat menjalankan docker melalui CLI Windows/Ubuntu
2. Masuk dalam directory web.
3. Jalankan perintah ini `docker build -t rolling_test .` di cli
4. Jalankan Perintah `docker run -d -p <port_yang_diinginkan>:3500 --name rolling rolling_test`
5. Jika tidak terjadi error saat menjalankan perintah nomor 4 artinya sudah selesai di buat.

## 2.b Menjalankan server menggunakan docker
1. Jika sudah menjalankan semua yang ada pada **1.b Instalasi**;
2. Akses `localhost:3500/`, jika tidak ada error maka akan tampil database yang ada

## 3. Library yang digunakan
* [KnexJS](https://knexjs.org/) - Query builder
* [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) - Library untuk generate jwt
* [md5](https://www.npmjs.com/package/md5`) - Library untuk generate hash password
* [crypto](https://www.npmjs.com/package/crypto) - Library untuk generate id
* [ExpressJS](https://expressjs.com/) - Framework untuk membuat API

## 4. Database yang digunakan
* [Postgresql 13.2](https://www.postgresql.org/about/news/postgresql-13-released-2077/) - Database SQL

## 5. Endpoint 

### Note: Pastikan anda melakukan login dengan cara memasukkan username dari info username dan password terlebih dahulu.
**Gunakan end point ini dengan cara menggabungkan dengan `localhost:3500`**

url| method | Body | Params | Querystring | Penjelasan
---|---|---|---|---|---
`/login`| POST | `{"username": "username_anda","password": "password dari user anda"}` | - | - | Untuk login 
`/gifts`|GET| - | `?page=<integer> (untuk parameter halaman yang diminta) , ?limit=<integer> (untuk parameter limit per tarikan data` | - | Untuk tarikan data semua gifts yang ada
`/gifts`|GET| - | - | `/:id_gifts` | Untuk mendapatkan detail 1 data gifts  
`/gifts`|POST| [Object](#object-post-gifts) | - | - | Untuk menambahkan data gifts
`/gifts`|PUT| [Object](#object-put-gifts) | - | `/:id_gifts` | Untuk mengubah data
`/gifts`|DELETE| - | - | `/:id_gifts` | Untuk menghapus data gifts 
`/gifts/:id_gifts/redeem `|POST| - | - | `/:id_gifts` | Untuk mereedem pada gifts tertentu untuk user yang telah mereedem
`/gifts/:id_gifts/id/redeem `|POST| [Object](#object-post-redeem) | - | `/:id_gifts` | Untuk memeberi rating pada gifts tertentu untuk users yang telah memiliki gifts tersebut


## Referensi
---
### Object Post Gifts
    {
        "nama_gifts": "Namaa",
        "detail_gifts":"Ini adalah contoh Detail",
        "kategori_gifts":"1",
        "id_profile_added_gifts":1,
        "is_available":1,
        "is_best_seller":0,
        "is_hot_item":0,
        "img_gifts": "",
        "stok":20
    }

### Object Put Gifts
    {
        "nama_gifts": "Namaa",
        "detail_gifts":"Ini adalah contoh Detail",
        "kategori_gifts":"1",
        "id_profile_added_gifts":1,
        "is_available":1,
        "is_best_seller":0,
        "is_hot_item":0,
        "img_gifts": "",
        "stok":20
    }

### Object Post Rating
    {
        "rating": 2.55
    }